<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// like /unlike
Route::match(['get','delete'],'/like/{id}','Homecontroller@like');
// Route::delete('/delete/{id}','Homecontroller@delete');
Route::match(['get','post'],'/comment/{id}','HomeController@comment');
