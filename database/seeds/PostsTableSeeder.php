<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'name'=>str_random(10).'post1',
            'user_id'=>'1'
        ]);
        DB::table('posts')->insert([
            'name'=>str_random(10).'post2',
            'user_id'=>'2'
        ]);
       
        DB::table('posts')->insert([
            'name'=>str_random(10).'post2',
            'user_id'=>'3'
        ]);
       
        DB::table('posts')->insert([
            'name'=>str_random(10).'post3',
            'user_id'=>'1'
        ]);
       
        DB::table('posts')->insert([
            'name'=>str_random(10).'post4',
            'user_id'=>'2'
        ]);
       
        DB::table('posts')->insert([
            'name'=>str_random(10).'post5',
            'user_id'=>'3'
        ]);
       
       
    }
}
