<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'user',
            'email'=>'user@mail.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@mail.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@mail.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@mail.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@mail.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@mail.com',
            'password'=>bcrypt('secret')
        ]);
    }
}
