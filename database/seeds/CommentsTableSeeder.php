<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'description'=>str_random(10),
            'user_id'=>'1',
            'post_id'=>'1'
        ]);
        DB::table('comments')->insert([
            'description'=>str_random(10),
            'user_id'=>'1',
            'post_id'=>'2'
        ]);
        DB::table('comments')->insert([
            'description'=>str_random(10),
            'user_id'=>'1',
            'post_id'=>'3'
        ]);
        DB::table('comments')->insert([
            'description'=>str_random(10),
            'user_id'=>'1',
            'post_id'=>'4'
        ]);
        DB::table('comments')->insert([
            'description'=>str_random(10),
            'user_id'=>'1',
            'post_id'=>'5'
        ]);
    }
}
