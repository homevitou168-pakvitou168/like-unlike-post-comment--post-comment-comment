<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Redirect;
use App\User;
use App\Like;
use App\Post;
use App\Comment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        $likes = Like::select('post_id')->where('user_id',Auth()->user()->id)->get();
        $likeArr=array_flatten($likes->toArray()); //convert multidimensional array to single array for easy access

        return view('/home',['posts'=>$posts,'likes'=>$likeArr]);
    }
    public function like($post_id,Request $request)
    {
        if(Auth()->check() || $post_id != null || Post::find($post_id) != null){
            if($request->isMethod('get'))
            {
                $user_id = Auth()->user()->id;
                $like = New Like();
                $like->user_id = $user_id;
                $like->post_id = $post_id;
                $like->save();
            }
            else
            {
                $likes = User::find(Auth()->user()->id)->likes;
                foreach( $likes as $like)
                {
                    if($like->post_id == $post_id)
                    {
                        Like::find($like->id)->delete();
                        return redirect('/home');
                    }
                }
            return redirect('/home');
            }
            return redirect('/home');
        }
    }
    function comment(Request $request,$post_id)
    {
        if($post_id != null || Post::Find($post_id) != null)
        {
            if($request->isMethod('get'))
            {
                return view('comment',['comments'=>Post::find($post_id)->comments,'post'=>Post::find($post_id)]);
            }
            else
            {
               $rules = ['comment'=>'required'];
               $this->validate($request,$rules);
               $comment = New Comment();
               $comment->description = $request->comment;
               $comment->user_id = Auth()->user()->id;
               $comment->post_id = $post_id;
               $comment->save();

               return redirect()->back();
            }
        }
        return redirect('/home');
    }
}
