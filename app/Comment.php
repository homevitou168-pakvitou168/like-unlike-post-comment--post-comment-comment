<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    // public $primaryKey = 'id';

    function post()
    {
        return $this->belongsTo(Post::class);
    }
    
    function user()
    {
        return $this->belongsTo(User::class);
    }
}
