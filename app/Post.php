<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    function Likes()
    {
        return $this->hasmany(Like::class);
    }
    function user()
    {
        return $this->belongsTo(User::class);
    }

    function comments()
    {
        return $this->hasMany(Comment::class);
    }

    
}
