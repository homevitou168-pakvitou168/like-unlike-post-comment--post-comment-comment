Step To run project:
1. clone
2. cd  /project's root
3. composer install 
4. copy .env.example .env  
5. config:database 
6. php artisan key:generate
7. php artisan migrate
8. php artisan db:seed  (to seed all seeder class)
9. localhost:8000


Done!

If you run step 8 then the system will generates 5 random users:

Default user:

    - email: user@mail.com
    - password:secret
    
    