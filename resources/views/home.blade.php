@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justtify-content-center">
        @foreach( $posts as $post)
        <div class="row col-md-8 offset-md-2 shadow-sm p-4 my-2">
            <div class="col-md-3 col-lg-2">
                <form action="{{url('/like/'.$post->id)}}" class="row m-0 p-0" method="POST" id="{{$post->id}}">
                        @if(in_array($post->id, $likes,true))
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="delete">
                            <a href="javascript:$('#{{$post->id}}').submit()" class="active btn btn-danger btn-sm">like</a> 
                        @else
                             <a href="{{url('/like/'.$post->id)}}" class="btn btn-primary btn-sm">Like</a> 
                        @endif
                </form>
            </div>
            <div class="col-md-8">
            <p class="text-danger font-weight"><a href="{{url('/comment/'.$post->id)}}">{{$post->name}}</a></p>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection