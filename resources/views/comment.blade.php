@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justtify-content-center">
        <div class="row col-md-8 offset-md-2 shadow-sm p-4 my-2">
            <div class="col-md-3 col-lg-2">
            </div>
            <div class="col-md-8">
            <p class="text-danger font-weight"><a href="">{{$post->name}}</a></p>
            </div>
        </div>
    </div>
    <p class=" row text-muted offset-md-2">Comments below:</p>
    <form action="{{url('/comment/'.$post->id)}}" method="POST" class="row col-md-8 offset-md-2 shadow-sm px-0">
        <div class="input-group">
            <div class="col">
                <input type="text" class="form-control" name="comment" value="">
            </div>
            {{csrf_field()}}
            <div class="input-group-btn">
                <input type="submit" class="btn btn-primary" value="Save">
            </div>
        </div>
    </form>
    <p class=" row text-muted offset-md-2 mt-5">All Comments:</p>

    <div class="row justtify-content-center">
        @foreach( $comments as $comment)
        <div class="row col-md-8 offset-md-2  p-4 my-2">
            <div class="col-md-3 col-lg-2">
            <strong>userID:#{{$comment->user_id}}</strong>
            </div>
            <div class="col-md-8">
                <p class="text-muted">
                    {{$comment->description}}    
                </p>
            </div>
        </div>
        <hr> 
        @endforeach
    </div>
</div>
@endsection